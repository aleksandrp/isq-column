# ISQ-column Operation Guide
If you are new to ISQ LC-MS and need access to it, please read the following guide.

### How to operate the machine
There will be a video soon (hopefully). For now, please take the **traininig** and
follow **operation procedure** section of this guide.

#### Video (youtube)
!TODO

#### Notes (FAQ)
 * PAY SPECIAL ATTENTION !! DO NOT SET BLANK AS TYPE:BLANK, keep the type as UNKNOWN.
 * Sample tray can be moved by hand when not injecting.
 * Mass-range is predefined to full operational range
 * keep attention to setting proper method
 * Stay away of pollutants!!

### How to gain access
 * Contact your MS tutor.
 * Read this webpage and the SOP
 * Measure few times with the MS tutor.
 * Request MS tutor to write me (Jan) and I will grant you access for the booking system.

### Booking system
[Link to the booking system](https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25)

### Known Pollutants
 * polar aprotic solvents (DMF, DMSO, HMPA..)
 * buffers (TEAB ..) and concentrated ionic/easily ionizable compounds (TEA, DIPEA..)
 * electrolytes (TBAF, TBAPF6, ...)
 * molecular sieves, microcrystalline compounds
 * low-grade solvents
 * concentrated samples
 * plastics, plasticizers
 * triphenylphosphine-oxide

### Operation procedure
 * Check if the solvent is there (> 200ml). If not, refill with pre-mix. If pre-mix is gone, contact me (Jan Zelenka, 03.409).
 * Insert your samples + blank.
 * Copy the standard sample template to your folder and personalize it.
 * Measure the samples.
 * Evaluate the samples.
 * Print / upload the outcome.
 * Set the sample flow low after finishing your sequence (will be automatic in future)
 * Check that the machine is clean once you finish.
 * Write a note into the logbook (its next to the machine, you will see notes from previous users there)

### Network storage
Machine is not connected to the internet at the moment. It will be conected once it will be at its new location.

### When something goes wrong
Contact your MS tutor! If MS tutor is not around contact some other MS tutor.

### List of MS Tutors
|Name                       |        Group           |
| -----------               | -----------            |
|Kolk, M.R. van der (Marnix)|       Eurostars        |
|Sondag, D. (Daan)          |       Boltje/Rutjes    |
|Meeusen, E. (Evy)          |       Boltje/Rutjes    |

Your group not in a list? **Become a MS tutor yourself!** (PhD preferred)


### Questions/Misc
 * If something is wrong in this guide feel free to contact me.

### TODO (planned improvements)
 * SOP is still missing
 * Explicitly show the type blank FAQ issue in the video !!



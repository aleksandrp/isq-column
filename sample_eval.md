# ISQ-column Sample evaluation guide
Quick evaluation guide


 * **Open the sample** of interest (double left click).<br>
<img src="gifs/opening_measurement.gif"><br><br><br>

 * **Select peaks** which you want to integrate (left click onto line and drag)<br>
<img src="gifs/peak_selection.gif"><br>
<img src="gifs/multiple_peaks.gif"><br><br><br>

 * You can see an **M/Z spectrum** of the scan which took place at the maximum of the 
   peak by clicking on an integrated peak (left click into the peak).<br>
<img src="gifs/show_spectra.gif"><br><br><br>
